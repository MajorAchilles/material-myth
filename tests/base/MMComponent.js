import MMComponent from "../../source/base/MMComponent";
import { assert } from "chai";

describe("The MMComponent class", () => {
    let mmComponent;

    beforeEach(() => {
        mmComponent = new MMComponent();
    });

    describe("The MMComponent.defaultProperties functions", () => {
        it("returns an object", () => {
            assert.isObject(mmComponent.defaultProperties(), "returned value is an object");
        });
    });

    describe("The MMComponent.defaultState functions", () => {
        it("returns an object", () => {
            assert.isObject(mmComponent.defaultState(), "returned value is an object");
        });
    });

    describe("The MMComponent.createChildren functions", () => {
        it("returns an array", () => {
            assert.isArray(mmComponent.createChildren(), "returned value is an array");
        });
    });
});
