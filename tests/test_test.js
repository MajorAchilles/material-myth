import { assert } from "chai";
import { cards } from "../source/api";

describe("A sample test suite", () => {
    it("should not fail", () => { assert(true); });
    it("card should be a function", () => { assert.isFunction(cards.SupportingTextCard.view, "Yay!"); });
});
