"use strict";

var eslintConfig = {
    "extends": "airbnb",
    "rules": {
        "import/no-extraneous-dependencies": "off",
        "import/no-unresolved": "off",
        "import/extensions": "off",
        "import/first": "off",
        "import/no-absolute-path": "off",
        "arrow-body-style": "off",
        "no-plusplus": "off",
        "no-underscore-dangle": "off",
        "brace-style": ["warn", "1tbs", { "allowSingleLine": true }],
        "max-len": ["warn", 180, 2, { "ignoreComments": true }],
        "indent": [
            "warn",
            4,
            {
                "SwitchCase": 1
            }
        ],
        "quotes": [
            "warn",
            "double"
        ],
        "no-constant-condition": ["error", { "checkLoops": false }],
        "comma-dangle": ["error", "never"],
        "no-param-reassign": ["error", { "props": false }],
        "class-methods-use-this": ["error", {
            "exceptMethods": [
                "defaultProperties",
                "defaultState",
                "createChildren",
                "view"
            ]
        }],
        "no-unused-vars": ["error", {
            "vars": "all",
            "args": "none"
        }],
        "new-cap": [
            "error",
            {
                "capIsNewExceptions": [
                    "SomeCapClass"
                ]
            }
        ]
    },
    "env": {
        "browser": true,
        "jasmine": true,
        "node": true
    },
    "root": true,
    "globals": {
        "SomeGlobalObject": true
    }
};

module.exports = Object.assign({}, eslintConfig);
