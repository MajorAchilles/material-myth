const {
    optimize: { UglifyJsPlugin }
} = require("webpack");
const path = require("path");
const environment = require("yargs").argv.env;

const MATERIAL_MYTH = "material-myth.js";
const MATERIAL_MYTH_MIN = "material-myth.min.js";
const MATERIAL_MYTH_TEST = "material-myth.tests.js";
const rootDir = process.cwd();

const loaders = [{
    test: /(\.js)$/,
    loader: "babel-loader",
    exclude: /node_module/
}];

const plugins = [];
const modules = [path.resolve(rootDir, "node_modules"), path.resolve(rootDir, "source")];
let outputFile = MATERIAL_MYTH;
let devtool = "source-map";
const entry = path.resolve(rootDir, "source/api.js");

if (environment === "build") {
    plugins.push(new UglifyJsPlugin({ minimize: true }));
    outputFile = MATERIAL_MYTH_MIN;
    devtool = undefined;
}

if (environment === "test") {
    outputFile = MATERIAL_MYTH_TEST;
}

module.exports = {
    entry,
    devtool,
    output: {
        path: `${rootDir}/lib`,
        filename: outputFile,
        library: "materialMyth",
        libraryTarget: "window"
    },
    module: {
        loaders
    },
    resolve: {
        modules,
        extensions: [".js", ".json"]
    },
    plugins
};
