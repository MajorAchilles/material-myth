/* eslint-disable no-console */
const fs = require("fs");
const path = require("path");

const sourceName = "codepoints";
const destinationName = "Icons.json";

const source = path.resolve(__dirname, sourceName);

fs.readFile(source, "utf-8", (readError, data) => {
    if (readError) {
        console.error(`Failed to read ${sourceName}: ${readError}`);
        return -1;
    }

    const iconNames = {};

    let counter = 0;
    data.split("\n")
        .map(line => line.split(" ")[0])
        .forEach((name) => {
            counter++;
            iconNames[name.toUpperCase()] = name;
        });

    console.log(iconNames);
    const destination = path.resolve(__dirname, destinationName);

    fs.writeFile(destination, JSON.stringify(iconNames), (writeError) => {
        if (writeError) {
            console.error(`Failed to write JSON to ${destinationName}: ${writeError}`);
            return -1;
        }

        console.log(`Wrote ${counter} icon names to ${destinationName}`);
        return 0;
    });

    return 0;
});

