import MMComponent from "../base/MMComponent";
import classNames from "classnames";
import NAME from "./Name";

const POSITION = {
    LEFT: "left",
    RIGHT: "right"
};

const SIZE = {
    TINY: "tiny",
    SMALL: "small",
    MEDIUM: "medium",
    LARGE: "large"
};

const materialIcons = "material-icons";

export default class Icon extends MMComponent {
    /**
     * @inheritDoc
     */
    defaultProperties() {
        return {
            customClassNames: "",
            defaultClassName: materialIcons,
            name: NAME.ADD,
            position: "",
            size: SIZE.MEDIUM
        };
    }

    /**
     * @inheritDoc
     */
    view(m, properties, state) {
        return m(
            "i",
            {
                class: classNames({
                    [properties.defaultClassName]: true,
                    [properties.size]: properties.size,
                    [properties.position]: properties.position,
                    [properties.classNames]: properties.classNames
                })
            },
            properties.name
        );
    }
}

Icon.POSITION = POSITION;

Icon.SIZE = SIZE;

Icon.NAMES = NAME;
