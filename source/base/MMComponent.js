/* eslint-disable */
import m from "mithril/hyperscript";
import { render as mRender } from "mithril/render";

export default class MMComponent { 
    constructor(properties, children) {
        this._children = [];
        this._namedChildren = {};
        this._parent = null;
        this._properties = {};
        this._state = {};
        this.initialize(properties, this.defaultProperties(), children);
    }

    /**
     * Defines the default properties of the component. These properties can be passed from outside.
     * @returns {Object} An object containing the default properties.
     */
    defaultProperties() {
        return {};
    }

    /**
     * Defines the default internal state of the component. These properties should only set set from within the component.
     * @returns {Object} An object representing the default internal state of the component.
     */
    defaultState() {
        return {};
    }

    /**
     * Creates the children of this component.
     * @returns {(Array<MMComponent>|Array<Object>)} Returns an array of components or an array of object containing named components.
     */
    createChildren() {
        return [];
    }

    /**
     * Containes the rendering logic for the component
     * @param {Function} m The mithril function
     * @param {Object} properties The properties of the component.
     * @param {Object} state The internal state of the component.
     * @param {Array<MMComponent>} children The array of child components.
     * @param {Object.<string, MMComponent>} namedChildren The object containing the named children.
     */
    view(m, properties, state, children, namedChildren) {
        return m("div", {}, "Override this function with your view logic");
    }

    initialize(defaultProperties, childProperties, children) {
        this._properties = Object.assign({}, defaultProperties, childProperties);
        this._children = children && children.concat(this.createChildren()) || this.createChildren();
        this._state = this.defaultState();
        this.render();
    }

    mount(elementID) {
        const element = document.querySelector(`#${elementID}`);

        if(!element) {
            new Error(`No element found with id: ${elementID}`);
            return;
        }

        mRender(element, this.render());
    }

    /**
     * Handles the render lifecycle
     * @returns {*} The 
     */
    render() {
        return this.view(m, this._properties, this._state, this._children, this._namedChildren);
    }

}
