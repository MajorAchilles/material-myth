const m = require("mithril");

/*
{
  icon: "work"
  line1: "Cerner Healthcare Solutions",
  line2: "2016-present"
} */
const mdl_list_twoLine = { // eslint-disable-line camelcase
    view(vnode) {
        return m(
            "ul",
            { class: "mdl-list" },
            m(
                "li",
                { class: "mdl-list__item mdl-list__item--two-line" },
                m(
                    "span",
                    { class: "mdl-list__item-primary-content" },
                    [
                        m("i", { class: "material-icons mdl-list__item-avatar" }, vnode.attrs.icon),
                        m("span", vnode.attrs.line1),
                        m("span", { class: "mdl-list__item-sub-title" }, vnode.attrs.line2)
                    ]
                )
            )
        );
    }
};


module.exports = {
    mdl_list_twoLine
};
