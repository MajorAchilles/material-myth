import m from "mithril";
import lists from "./list/index";
import cards from "./cards/index";
import Icon from "./icons/Icon";

/*
{
  children: []
} */
const mdl_cell = { // eslint-disable-line camelcase
    view(vnode) {
        return m(
            "div",
            { class: `mdl-cell ${vnode.attrs.cellClasses}` },
            vnode.attrs.children
        );
    }
};

/*
{
  children: []
} */
const mdl_grid = { // eslint-disable-line camelcase
    view(vnode) {
        return m(
            "div",
            { class: "mdl-grid" },
            vnode.attrs.children
        );
    }
};

/*
{
  children: []
} */
const page_content = { // eslint-disable-line camelcase
    view(vnode) {
        return m(
            "div",
            { class: "page-content" },
            vnode.attrs.children
        );
    }
};

export {
    cards,
    Icon,
    lists,
    mdl_cell, // eslint-disable-line camelcase
    mdl_grid, // eslint-disable-line camelcase
    page_content // eslint-disable-line camelcase
};
