const m = require("mithril");

/*
{
  text: "Sample text."
}
   OR
{
  children: []
} */
const mdl_card_supportingText = { // eslint-disable-line camelcase
    view: vnode => m("div", { class: "mdl-card__supporting-text" }, vnode.attrs.text || vnode.attrs.children)
};

module.exports = {
    mdl_card_supportingText
};
