const m = require("mithril");
const { mdl_card_supportingText } = require("./supportingTextCard");
const { mdl_list_twoLine } = require("./../list/twoLineList");


/*
{
icon: "work",
place: "Cerner Healthcare Solutions",
title: "Software Engineer",
period: "2016 - Present",
} */
const mdl_card_twoLine = { // eslint-disable-line camelcase
    view(vnode) {
        return m(
            "div",
            { class: "am-full-width-card mdl-card mdl-shadow--2dp" },
            [
                m(
                    "div",
                    { class: "mdl-card__title" },
                    m(
                        "h2",
                        { class: "mdl-card__title-text" },
                        vnode.attrs.title
                    )
                ),
                m(mdl_card_supportingText, {
                    children: [
                        m(mdl_list_twoLine, {
                            icon: vnode.attrs.icon,
                            line1: vnode.attrs.place,
                            line2: vnode.attrs.period
                        })
                    ]
                })
            ]
        );
    }
};


module.exports = {
    mdl_card_twoLine
};
