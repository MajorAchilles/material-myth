const m = require("mithril");
const { mdl_card_supportingText } = require("./supportingTextCard");


/*
{
  squareClass: "am-card-square-devil",
  icon: "pet",
  title: "Being Inhuman",
  subtitle: "Not a people person. Definitely not."
} */
const mdl_card_square = { // eslint-disable-line camelcase
    view(vnode) {
        return m(
            "div",
            { class: `am-card-square mdl-card mdl-shadow--2dp ${vnode.attrs.squareClass}` },
            [
                m(
                    "div",
                    { class: "mdl-card__title mdl-card--expand" },
                    m(
                        "h2",
                        { class: "mdl-card__title-text  am-header-icon" },
                        [
                            m(
                                "i",
                                { class: "material-icons mdl-list__item-avatar" },
                                vnode.attrs.icon
                            ),
                            m("span", vnode.attrs.title)
                        ]
                    )
                ),
                m(mdl_card_supportingText, { text: vnode.attrs.subtitle })
            ]
        );
    }
};


module.exports = {
    mdl_card_square
};
