const { Icon } = window.materialMyth;

const mountMyth = () => {
    new Icon().mount("root"); // eslint-disable-line no-undef
};

document.body.onload = mountMyth;
