# Material Myth

[![Build](https://gitlab.com/MajorAchilles/material-myth/badges/master/build.svg)](https://gitlab.com/MajorAchilles/material-myth/pipelines)

[![License](https://img.shields.io/:license-mit-blue.svg)](https://gitlab.com/MajorAchilles/material-myth/blob/master/LICENSE)

A [Mithril](https://github.com/MithrilJS/mithril.js) and [Materialize](https://github.com/Dogfalo/materialize) based rendering framework.

## Installation
```npm install material-myth --save```

## Demo Project
* material-myth/demo/demo.html
* [yawa-electron](https://gitlab.com/MajorAchilles/yawa_electron)

## Yet to do
* Injector for `materialize` css.
* OO-fy the the architecture.
* Base `mmComponent`. (In progress)
* Unit tests

## Components
* Badges `Not started`
    * Collections
    * Badges in Dropdowns
    * Badges in Navbar
    * Badges in Collapsibles
    * Options
* Buttons `Not started`
    * Raised
    * Floating
    * Flat
    * Submit
    * Large
    * Disabled
* Breadcrumbs `Not started`
* Cards `Not started`
    * Basic Card
    * Image Card
    * Floating Action Button Card
    * Horizontal Card
    * Card Reveal
    * Tab Cards
    * Card Sizes
    * Card Panels
* Chips `Not started`
* Collection `Not started`
    * Basic
    * Links
    * Headers
    * Secondary
    * Avatar
    * Dismissible
* Footer `Not started`
    * Basic
    * Sticky
* Forms `Not started`
    * Text Input
    * TextArea
    * Select
    * Radio Buttons
    * Checkboxes
    * Switches
    * File Input
    * Range Slider
    * Date Picker
    * Time Picker
    * Character Counter
    * Autocomplete
* Icons `Done`
* Navbar `Not started`
    * Basic Navbar
    * Right/Left/Center Aligned Links
    * Logo
    * Active Items
    * Tabs
    * Navbar Dropdown
    * Icon Links
    * Navbar Buttons
    * Search Bar
    * Hamburger Menu
* Pagination `Not started`
* Loaders `Not started`
    * Linear
    * Circular - Basic
    * Circular - Dynamic Colors
    